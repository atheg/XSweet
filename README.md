XSweet - XSLT for Pubsweet
Including extraction of document contents from MS Office Open XML into HTML

Wendell Piez for Coko Foundation
From July 2016

See the project Wiki at https://gitlab.coko.foundation/wendell/XSweet/wikis/home for more info.